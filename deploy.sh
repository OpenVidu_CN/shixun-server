#!/bin/bash
PROG_NAME=$0
ACTION=$1
OVSECRET=$2
APP_NAME=shixun
APP_HOME=/home/www/${APP_NAME}  # 从package.tgz中解压出来的jar包放到这个目录下
APP_LOG=${APP_HOME}/logs/app.log # 应用的日志文件
# 创建出相关目录

mkdir -p ${APP_HOME}
mkdir -p ${APP_HOME}/logs
usage() {
    echo "Usage: $PROG_NAME {start|stop|restart}"
    exit 2
}
start() {
    echo 'sleep 6'
    sleep 6
    echo 'starting ...'
    PUBLIC_IP=$(curl -4 https://icanhazip.com 2>/dev/null)
    PARAM="-Dopenvidu.url=PUBLIC_IP -Dopenvidu.secret=${OVSECRET}"
    JAVA_OPTS="-XX:+PrintGCDetails -XX:+PrintGCDateStamps -Xloggc:${APP_HOME}/logs/gc.$$.log -Djava.security.egd=file:/dev/./urandom -Xms256m -Xmx256m  -XX:MetaspaceSize=64m -XX:MaxMetaspaceSize=64m -XX:+UseConcMarkSweepGC -Xmn128m -Xss328k"
    nohup java ${JAVA_OPTS} ${PARAM} -jar ${APP_HOME}/${APP_NAME}.jar 2>&1 &
}
stop() {
    echo 'stopping ...'
    PID=`ps ax | grep ${APP_NAME} | grep -v grep | awk '{print $1}'`
    echo ${PID}
    if [[ ! -z "$PID" ]]; then
    kill -15 $PID
    else
     echo 'stopping fail'
    fi
}
case "$ACTION" in
    start)
        start
    ;;
    stop)
        stop
    ;;
    restart)
        stop
        start
    ;;
    *)
        usage
    ;;
esac