package com.easy.lab.shixun.api.bo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.12 2019
 */
@ApiModel
@Data
public class OpenviduRequest {
    @ApiModelProperty(value = "群唯一编码")
    String groupUniqueId;
    @ApiModelProperty(value = "录制启动停止")
    Boolean state;
}
