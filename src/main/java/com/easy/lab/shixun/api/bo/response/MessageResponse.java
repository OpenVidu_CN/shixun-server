package com.easy.lab.shixun.api.bo.response;

import com.easy.lab.shixun.api.bo.MessageBase;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 返回消息
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
public class MessageResponse extends MessageBase {

    /**
     * 消息指定接受者识别号
     */
    private String toPrincipal;

    /**
     *  是否群发消息
     *  默认群发
     */
    private boolean isToGroup = true;
}
