package com.easy.lab.shixun.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * 方便本地测试用，线上应该动静分离
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.15 2019
 */
@Controller
public class IndexController {

    @RequestMapping(value = "/", method = { RequestMethod.GET})
    public String index(Model model, HttpServletRequest request) {
        return "index.html";
    }
}
