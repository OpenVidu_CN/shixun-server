package com.easy.lab.shixun.controller;

import com.easy.lab.shixun.api.bo.request.MessageRequest;
import com.easy.lab.shixun.service.manager.MessageManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 消息相关接口
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.07 2019
 */
@Api(tags = "消息相关接口")
@RestController
@Slf4j
@RequestMapping("/msg")
public class MessageController {

    @Autowired
    private MessageManager messageManager;

    @ApiOperation("发送消息接口")
    @MessageMapping("/send")
    public void send(@RequestBody MessageRequest requestBody) {
        log.info("requestBody = {}", requestBody);
        messageManager.processMessage(requestBody);
    }
}
