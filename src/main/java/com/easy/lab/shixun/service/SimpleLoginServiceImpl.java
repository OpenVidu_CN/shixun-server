package com.easy.lab.shixun.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.easy.lab.shixun.api.bo.request.LoginRequest;
import com.easy.lab.shixun.api.bo.response.TokenInfoResponse;
import com.easy.lab.shixun.api.service.LoginService;
import com.easy.lab.shixun.common.utils.RandomStringUtil;
import com.easy.lab.shixun.dao.dataobject.SxMember;
import com.easy.lab.shixun.dao.mapper.SxMemberMapper;
import com.easy.lab.shixun.service.manager.SimpleCoreManager;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 登录接口简单实现
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.08 2019
 */
@Service
public class SimpleLoginServiceImpl implements LoginService {

    @Autowired
    private SxMemberMapper sxMemberMapper;

    @Autowired
    private SimpleCoreManager simpleCoreManager;

    @Override
    public TokenInfoResponse login(LoginRequest loginParam) {
        if (Objects.isNull(loginParam)) {
            return getUnLoginResultBO();
        }

        TokenInfoResponse tokenInfoResponse = checkLogin(loginParam.getToken());
        // 未登录前置操作
        if (Objects.isNull(tokenInfoResponse) || !tokenInfoResponse.getIsLogin()) {
            if (StringUtils.isBlank(loginParam.getUsername()) || StringUtils.isBlank(loginParam.getPassword())) {
                return getUnLoginResultBO();
            }
            // 帐号校验
            SxMember sxMember = sxMemberMapper.selectOne(new QueryWrapper<SxMember>().lambda()
                    .eq(SxMember::getMemberUniqueId, loginParam.getUsername())
                    .eq(SxMember::getMemberPass, loginParam.getPassword())
            );
            if (sxMember == null) {
                return getUnLoginResultBO();
            }
            tokenInfoResponse = new TokenInfoResponse();
            String token = RandomStringUtil.createToken();
            tokenInfoResponse.setToken(token);
            tokenInfoResponse.setMemberUniqueId(sxMember.getMemberUniqueId());
            tokenInfoResponse.setMemberNickname(sxMember.getMemberNickname());
            tokenInfoResponse.setIsLogin(true);
            setLoginResult(token, tokenInfoResponse);
        }
        return tokenInfoResponse;
    }

    private TokenInfoResponse getUnLoginResultBO() {
        TokenInfoResponse tokenInfoResponse = new TokenInfoResponse();
        tokenInfoResponse.setIsLogin(false);
        return tokenInfoResponse;
    }

    @Override
    public TokenInfoResponse logout(LoginRequest loginParam) {
        if (Objects.nonNull(loginParam) && Objects.nonNull(loginParam.getToken())) {
            simpleCoreManager.removeToken(loginParam.getToken());
        }
        return null;
    }

    @Override
    public TokenInfoResponse checkLogin(String token) {
        if (Objects.isNull(token)) {
            return null;
        }
        return simpleCoreManager.getReadOnlyTokenInfo().get(token);
    }

    @Override
    public void setLoginResult(String token, TokenInfoResponse tokenInfoResponse) {
        simpleCoreManager.addTokenInfo(token, tokenInfoResponse);
    }

    @Override
    public List<TokenInfoResponse> getInfosByTokens(List<String> tokens) {
        List<TokenInfoResponse> tokenInfoResponses = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(tokens)) {
            simpleCoreManager.getReadOnlyTokenInfo().forEach((key,tokenInfoBO) -> {
                if (tokens.contains(key)) {
                    tokenInfoResponses.add(tokenInfoBO);
                }
            });
        }
        return tokenInfoResponses;
    }

}
