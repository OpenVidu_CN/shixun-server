package com.easy.lab.shixun.service.processor;

import com.easy.lab.shixun.api.bo.request.MessageRequest;
import com.easy.lab.shixun.api.bo.response.MessageResponse;

/**
 * 事件处理器接口
 * @Author: https://gitee.com/wesleyOne
 * @Date: 12.15 2019
 */
public interface BaseEventProcessor {

    /**
     * 事件消息处理
     * @param requestBody   消息请求
     * @return              消息回复
     */
    MessageResponse process(MessageRequest requestBody);
}
